$(function() {

 $("#owl-example").owlCarousel({
 	items: 1,
 	autoplay:true,
    autoplayTimeout: 7500
 });


$('.main_nav a').click(function(){
		$('.main_nav a').removeClass('active');
    $(this).toggleClass('active');
   });

     
	
$('.serv_item').equalHeights();

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });
	
});

$(window).load(function() {

	$(".loader_inner").fadeOut();
	$(".loader").delay(400).fadeOut("slow");

});
